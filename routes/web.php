<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@home')->name('home');
Route::get('/movies', 'HomeController@movies')->name('movies');

Route::resource('admin/halls', 'Admin\\HallsController');
Route::resource('admin/movies', 'Admin\\MoviesController');
Route::resource('admin/movies', 'Admin\\MoviesController');
Route::resource('move/movies', 'Movies\\MoviesController');
Route::resource('admin/cashiers', 'Admin\\CashiersController');