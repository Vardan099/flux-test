<?php

use Illuminate\Database\Seeder;
use App\Role as Role;
use App\User as User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_employee = Role::where('name', 'employee')->first();
        $role_cashier  = Role::where('name', 'cashier')->first();
        $role_admin  = Role::where('name', 'admin')->first();

        $employee = new User();
        $employee->name = 'Employee';
        $employee->email = 'employee@example.com';
        $employee->password = bcrypt('employee123');
        $employee->save();
        $employee->roles()->attach($role_employee);

        $cashier = new User();
        $cashier->name = 'Cashier';
        $cashier->email = 'cashier@example.com';
        $cashier->password = bcrypt('cashier123');
        $cashier->save();
        $cashier->roles()->attach($role_cashier);

        $admin = new User();
        $admin->name = 'Admin';
        $admin->email = 'admin@example.com';
        $admin->password = bcrypt('admin123');
        $admin->save();
        $admin->roles()->attach($role_admin);



  }
}
