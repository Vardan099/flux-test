<div class="col-md-3">
    <div class="panel panel-default panel-flush">
        <div class="panel-heading">
            Sidebar
        </div>

        <div class="panel-body">
            <ul class="nav" role="tablist">
                <li role="presentation">
                    <a href="{{ url('/admin/halls') }}">
                        Halls
                    </a>
                </li>
                <li role="presentation">
                    <a href="{{ url('/admin/cashiers') }}">
                        Cashiers
                    </a>
                </li>
                <li role="presentation">
                    <a href="{{ url('/admin/movies') }}">
                        Movies
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
