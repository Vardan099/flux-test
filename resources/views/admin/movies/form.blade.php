<div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
    <label for="title" class="col-md-4 control-label">{{ 'Title' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="title" type="text" id="title" value="{{ $movie->title or ''}}" required>
        {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('start_time') ? 'has-error' : ''}}">
    <label for="start_time" class="col-md-4 control-label">{{ 'Start Time' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="start_time" type="time" id="start_time" value="{{ $movie->start_time or ''}}" required>
        {!! $errors->first('start_time', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('day') ? 'has-error' : ''}}">
    <label for="day" class="col-md-4 control-label">{{ 'Day' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="day" type="date" id="day" value="{{ $movie->day or ''}}" required>
        {!! $errors->first('day', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('duration') ? 'has-error' : ''}}">
    <label for="duration" class="col-md-4 control-label">{{ 'Duration' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="duration" type="number" id="duration" value="{{ $movie->duration or ''}}" required>
        {!! $errors->first('duration', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('price') ? 'has-error' : ''}}">
    <label for="price" class="col-md-4 control-label">{{ 'Price' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="price" type="text" id="price" value="{{ $movie->price or ''}}" required>
        {!! $errors->first('price', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
    <label for="image" class="col-md-4 control-label">{{ 'Image' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="image" type="file" id="image" value="{{ $movie->image or ''}}" >
        {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('hall_id') ? 'has-error' : ''}}">
    <label for="hall_id" class="col-md-4 control-label">{{ 'Select a hall' }}</label>
    <div class="col-md-6">
        <select class="form-control" name="hall_id" type="number" id="hall_id" value="{{ $movie->hall_id or ''}}" >
            @foreach($halls as $hall)
                <option value="{{$hall->id}}">{{$hall->title}}</option>
            @endforeach
        </select>
        {!! $errors->first('hall_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        <input class="btn btn-primary" type="submit" value="{{ $submitButtonText or 'Create' }}">
    </div>
</div>
