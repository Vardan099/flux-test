@extends('layouts.app')

@section('content')
    <style>
        .image-movie-show{

        }
    </style>
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">Movie {{ $movie->id }}</div>
                    <div class="panel-body">

                        <a href="{{ url('/move/movies') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/move/movies/' . $movie->id . '/edit') }}" title="Edit Movie"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('move/movies' . '/' . $movie->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-xs" title="Delete Movie" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        <br/>
                        <br/>

                        <img class="col-md-6" src="/uploads/movies/{{$movie->image}}" style="max-width: 50%;margin: 0 auto;">

                        <div class="table-responsive col-md-6">
                            <table class="table table-borderless">
                                <tbody>
                                <tr>
                                    <th>ID</th>
                                    <td>{{ $movie->id }}</td>
                                </tr>
                                <tr>
                                    <th> Title</th>
                                    <td> {{ $movie->title }} </td>
                                </tr>
                                <tr>
                                    <th> Start Time</th>
                                    <td> {{ $movie->start_time }} </td>
                                </tr>
                                <tr>
                                    <th> Day</th>
                                    <td> {{ $movie->day }} </td>
                                </tr>
                                <tr>
                                    <th> Price</th>
                                    <td> {{ $movie->price }} </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
