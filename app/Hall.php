<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hall extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'halls';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['title'];

    public function user()
	{
		return $this->belongsTo('App\User');
	}

	public function movies(){
        return $this->belongsToMany('App\Movie','halls_movies','hall_id','movie_id');
    }
	
}
