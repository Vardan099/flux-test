<?php

namespace App\Http\Controllers\Movies;

use App\Hall;
use App\HallsMovies;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Movie;
use Illuminate\Http\Request;

class MoviesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $movies = Movie::where('title', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $movies = Movie::paginate($perPage);
        }

        return view('admin.movies.index', compact('movies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $halls = Hall::all();
        return view('admin.movies.create',compact('halls'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'title' => 'required',
			'start_time' => 'required',
			'day' => 'required',
			'duration' => 'required',
			'price' => 'required'
		]);
        $requestData = $request->all();
        

        if ($request->hasFile('image')) {
                $uploadPath = public_path('/uploads/movies');
                $extension = $request['image']->getClientOriginalExtension();
                $fileName = rand(11111, 99999) . '.' . $extension;
                $request['image']->move($uploadPath, $fileName);
                $requestData['image'] = $fileName;
        }

        $move = Movie::create($requestData);
        HallsMovies::create(['hall_id'=>$requestData['hall_id'],'movie_id'=>$move->id]);

        return redirect('move/movies')->with('flash_message', 'Movie added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $movie = Movie::findOrFail($id);

        return view('admin.movies.show', compact('movie'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $movie = Movie::findOrFail($id);
        $halls = Hall::all();
        return view('admin.movies.edit', compact('movie','halls'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'title' => 'required',
			'start_time' => 'required',
			'day' => 'required',
			'duration' => 'required',
			'price' => 'required'
		]);
        $requestData = $request->all();
        

        if ($request->hasFile('image')) {
            foreach($request['image'] as $file){
                $uploadPath = public_path('/uploads/image');

                $extension = $file->getClientOriginalExtension();
                $fileName = rand(11111, 99999) . '.' . $extension;

                $file->move($uploadPath, $fileName);
                $requestData['image'] = $fileName;
            }
        }

        $movie = Movie::findOrFail($id);
        $movie->update($requestData);

        return redirect('move/movies')->with('flash_message', 'Movie updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Movie::destroy($id);

        return redirect('move/movies')->with('flash_message', 'Movie deleted!');
    }
}
