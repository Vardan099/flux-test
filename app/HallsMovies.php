<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HallsMovies extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'halls_movies';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['hall_id','movie_id'];
}
